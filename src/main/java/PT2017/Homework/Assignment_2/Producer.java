package PT2017.Homework.Assignment_2;

import java.util.concurrent.BlockingQueue;
import java.util.*;

public class Producer implements Runnable {

	private final BlockingQueue<Client> clients;
	private long runtime;
	private int ID = 1; 
	private int min; 
	private int max;
	private final static int MAX_NR_CLIENTS = 10;
	String txt = new String();

	
	public Producer (BlockingQueue<Client> queue, int runtime, int min, int max){
		this.clients = queue;
		this.runtime = (new Date().getTime() + runtime*1000);
		this.max = max;
		this.min = min;
	}
	
	//method for "producing" clients. After a client has been added to the queue the thread sleeps a random number of seconds
	//before adding another client. Also as long as the queue is full the producer stalls
	public void run(){
		try{
			while(new Date().getTime() < this.runtime){
				if (isFull()){
					continue;
				}	
				Thread.sleep(returnRandom(min, max)*1000);
				clients.put(produce());
			}
		} catch (InterruptedException e){
			
		}
	}
	
	//check if the queue is full 
	private boolean isFull(){
		if (clients.size() == MAX_NR_CLIENTS){
			return true;
		}
		return false;
	}
	
	public long getRuntime(){
		return runtime;
	}
	
	//random min < number < max generator
	private int returnRandom(int min, int max){
		Random rand = new Random(); 
		int randomval = rand.nextInt((max - min) + 1) + min; 
		return randomval;
	}
	
	//creates a new client in the queue
	private Client produce() throws InterruptedException{
		Client client = new Client(ID, new Date());
		txt = txt + "Client " + client.getID() + " has been added to the queue";
		ID++;
		return client;
	}
	
	public BlockingQueue<Client> getQueue(){
		return this.clients;
	}
	
}
	