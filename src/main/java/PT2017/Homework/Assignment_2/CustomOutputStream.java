package PT2017.Homework.Assignment_2;
import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;

public class CustomOutputStream extends OutputStream{
	
	private JTextArea textArea;
	
	public CustomOutputStream(JTextArea textArea){
		this.textArea = textArea;
	}
	//This is the method for redirecting the printstream from the console to the TextArea.
	public synchronized void write(int b) throws IOException{
		textArea.append(String.valueOf((char)b));
		textArea.setCaretPosition(textArea.getDocument().getLength());
		textArea.update(textArea.getGraphics());
	}
	
}
