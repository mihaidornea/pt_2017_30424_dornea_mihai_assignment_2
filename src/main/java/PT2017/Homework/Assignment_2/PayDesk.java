package PT2017.Homework.Assignment_2;

import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.BlockingQueue;

import javax.swing.JTextArea;

public class PayDesk implements Runnable{
private final BlockingQueue<Client> clients;
private long runtime;
private int id;
private int nrClients;
private long meanWaitingTime;
private int max; 
private int min;
private float maximumWaitTime;
private float emptyTime;
private volatile String txt = new String();
private volatile JTextArea ta;

	//Constructor for the paydesk
	public PayDesk(BlockingQueue<Client> queue, int runtime, int id, int min, int max, JTextArea ta){
		this.clients = queue;
		this.runtime = (new Date().getTime() + runtime*1000);
		this.id = id;
		this.max = max;
		this.min = min;
		this.ta = ta;
		PrintStream printStream = new PrintStream(new CustomOutputStream(ta));
		System.setOut(printStream);
	}
	
	//The overriden run method that is running until the runtime condition is met
	//This method "consumes" clients by removing them from the blocking queue and then sleeps for a random time between a min and a max
	//representing the "serve time"
	public void run() {
		try{
			while (new Date().getTime() < this.runtime){
				while (isEmpty()){
					emptyTime++;
					Thread.sleep(1);
				}
				Thread.sleep(returnRandom(min, max)*1000);
				consume((Client) clients.take());
				
			}
		}catch (InterruptedException e){
			
		}
	}
	
	//Verify if queue is empty
	private boolean isEmpty(){
		if (clients.size() == 0)
			return true;
		return false;
	}
	
	//returns a string containing information on how much the queue was empty
	public String getEmptyTime(){
		return new String("Pay Desk " + id + " was empty for a total of " + emptyTime/1000 + " seconds");
	}
	
	//method to calculate the maximum waiting time for the queue
	public void findMaximumWait(long time){
		if (time > maximumWaitTime){
			maximumWaitTime = time/1000;
		}
	}

	public float getMaximumWaitTime(){
		return this.maximumWaitTime;
	}
	
	//method to calculate the mean waiting time per queue
	public float getMeanWaitingTime(){
		float meanSeconds = meanWaitingTime/1000;
		return (meanSeconds/nrClients);
	}
	
	//random number between a minimum and a maximum generator
	private int returnRandom(int min, int max){
		Random rand = new Random(); 
		int randomval = rand.nextInt((max - min) + 1) + min; 
		return randomval;
	}
	
	//This method is used for printing in real time in the TextArea (LOG) from the GUI by redirecting the printstream from the console 
	//to the TextArea "ta"
	//In order to avoid as much as I can overlaping strings, random small sleep time was added 
	private synchronized void printIt(Client c){
		txt = "Client " + c.getID() + " has been served by PayDesk " + id;
		System.out.println(txt);
		
	}
	
	//deletes a client from the blocking queue and calculates how much he waited in the queue
	private void consume(Client c){
		c.setDepartureTime(new Date().getTime());
		nrClients++;
		this.meanWaitingTime += (c.getDepartureTime() - c.getArrivalTime());
		findMaximumWait(c.getDepartureTime()-c.getArrivalTime());
		printIt(c);
	}

	public String howManyClients(){
		return new String("PayDesk " + this.id + " has served " + nrClients + " clients");
	}
	
	public String meanServeTime(){
		return new String("At PayDesk " + this.id + " the mean waiting time was " + getMeanWaitingTime());
	}

	public String emptyTime(){
		return new String("The maximum waiting time for PayDesk " + getId() + " was " + Float.toString(getMaximumWaitTime()));
	}
	
	public int getId() {
		return id;
	}
	
}