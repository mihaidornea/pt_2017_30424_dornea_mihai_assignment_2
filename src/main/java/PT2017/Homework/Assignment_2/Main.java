package PT2017.Homework.Assignment_2;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;


public class Main extends JFrame {

	private JPanel contentPane;
	private static JTextField txtInsertTheAmount;
	private static JTextField txtInsertTheHour;
	private static JTextArea textArea_3 = new JTextArea();
	private int nrPaydesk = 0; 
	private int runtime; 
	private int minServeTime = 0;
	private int maxServeTime = 0;
	private int minSpawnTime = 0;
	private int maxSpawnTime = 0;
	private int hour;
	private int idPaydesk = 1;
	private SwingWorker<Boolean, String> worker;
	private JScrollPane scrollPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JCheckBox chckbxSimulateWithUser = new JCheckBox("Simulate with user parameters");
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("QueueSimulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 953, 779);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtInsertTheAmount = new JTextField();
		txtInsertTheAmount.setHorizontalAlignment(SwingConstants.CENTER);
		txtInsertTheAmount.setBounds(111, 25, 278, 20);
		txtInsertTheAmount.setToolTipText("Insert the amount of time you want the simulation to run");
		contentPane.add(txtInsertTheAmount);
		txtInsertTheAmount.setColumns(10);
		
		txtInsertTheHour = new JTextField();
		txtInsertTheHour.setToolTipText("Insert the hour you want to simulate");
		txtInsertTheHour.setHorizontalAlignment(SwingConstants.CENTER);
		txtInsertTheHour.setBounds(440, 25, 278, 20);
		contentPane.add(txtInsertTheHour);
		txtInsertTheHour.setColumns(10);
		
		
		JButton btnNewButton = new JButton("Start Simulation");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtInsertTheAmount.getText().equals("") /*|| txtInsertTheHour.getText().equals("")*/){
				PrintStream printStream = new PrintStream(new CustomOutputStream(textArea_3));
				System.setOut(printStream);
				System.out.println("Incorrect input or incomplete input");
				} else 	if (getCheck().isSelected()){
					if (txtInsertTheAmount.getText().equals("")||textField.getText().equals("")||textField_1.getText().equals("") || textField_2.getText().equals("")|| textField_3.getText().equals("") || textField_4.getText().equals("")){
						PrintStream printStream = new PrintStream(new CustomOutputStream(textArea_3));
						System.setOut(printStream);
						System.out.println("Incorrect input or incomplete input");
						} else startWithParam();
				}
					else started();
			}
		});
				
		btnNewButton.setBounds(771, 36, 134, 23);
		contentPane.add(btnNewButton);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 104, 822, 625);
		contentPane.add(scrollPane);
		scrollPane.setViewportView(textArea_3);
		textArea_3.setBackground(Color.WHITE);
		
		JLabel runtimeLabel = new JLabel("Insert the amount of time you want to simulate for");
		runtimeLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		runtimeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		runtimeLabel.setBounds(97, 0, 298, 14);
		contentPane.add(runtimeLabel);
		
		JLabel lblInsertAHour = new JLabel("Insert a hour at which you simulate (7-22)");
		lblInsertAHour.setHorizontalAlignment(SwingConstants.CENTER);
		lblInsertAHour.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblInsertAHour.setBounds(440, 0, 278, 14);
		contentPane.add(lblInsertAHour);
		
		
		chckbxSimulateWithUser.setBounds(748, 6, 171, 23);
		contentPane.add(chckbxSimulateWithUser);
		
		JLabel lblNumberOfPaydesks = new JLabel("Number of paydesks");
		lblNumberOfPaydesks.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumberOfPaydesks.setBounds(31, 56, 124, 14);
		contentPane.add(lblNumberOfPaydesks);
		
		textField = new JTextField();
		textField.setBounds(57, 73, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblMinimumAndMaximum = new JLabel("Minimum and maximum client spawn time");
		lblMinimumAndMaximum.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinimumAndMaximum.setBounds(181, 58, 251, 14);
		contentPane.add(lblMinimumAndMaximum);
		
		textField_1 = new JTextField();
		textField_1.setBounds(212, 73, 40, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(362, 73, 40, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblMinimumAndMaximum_1 = new JLabel("Minimum and maximum serve time");
		lblMinimumAndMaximum_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinimumAndMaximum_1.setBounds(504, 56, 201, 14);
		contentPane.add(lblMinimumAndMaximum_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(495, 73, 40, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(678, 73, 40, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
	}
	
	private void startWithParam(){
		
		
			worker = new SwingWorker<Boolean, String>(){
			private PayDesk[] consumers;
			private Producer[] producers;
			private Thread[] threads;
			private int paydesks;
			private int threadnr;
			
			@Override
			protected Boolean doInBackground() throws Exception {

				int runtime = getRuntime();
				
				minServeTime = Integer.parseInt(getTxt3().getText());
				maxServeTime = Integer.parseInt(getTxt4().getText());
				minSpawnTime = Integer.parseInt(getTxt1().getText());
				maxSpawnTime = Integer.parseInt(getTxt2().getText());
				paydesks = Integer.parseInt(getTxt().getText());
				int paydeskID = 1;
				threadnr = 2*paydesks;
				
				
				JTextArea ta;
				ta = textArea_3;
				consumers = new PayDesk[paydesks];
				producers = new Producer[paydesks];
				threads = new Thread[threadnr];
				for (int i = 0; i < paydesks; i++){
					producers[i] = new Producer(new ArrayBlockingQueue<Client>(100), runtime, minSpawnTime, maxSpawnTime);
					consumers[i] = new PayDesk(producers[i].getQueue(), runtime, paydeskID, minServeTime, maxServeTime, ta);
					paydeskID++;
					}
			
				
				for (int i = 0; i < paydesks; i++){
					threads[i] = new Thread(producers[i]);		
					threads[i].start();
				}
				int j = 0;
				for (int i = paydesks; i < threadnr; i++){
					threads[i] = new Thread(consumers[j]);		
					threads[i].start();
					j++;
				}
				
				return true;
			}
			
			@Override
			protected void done() {
			
				String txt3 = new String();
				
				for (int i = 0; i < threadnr; i++){
					try {
						threads[i].join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			
				for (int i = 0 ; i < paydesks; i++){
				
						txt3 = txt3 + consumers[i].howManyClients() + "\n" + consumers[i].meanServeTime() + "\n" + consumers[i].emptyTime() + "\n\n"; 
				}				
				setTA3(txt3); 
				super.done();
			}		
		};
		worker.execute();
		
	}
	
	private void started(){
		
		worker = new SwingWorker<Boolean, String>() {

			private PayDesk[] consumers;
			private Producer[] producers;
			private Thread[] threads;
			private int paydesks;
			private int threadnr;
			 
			@Override
			protected Boolean doInBackground() throws Exception {
								
				int hour = getHour();
				int runtime = getRuntime();
				paydesks = 0;
				int paydeskID = 1;
				
				switch (hour){ 
				case 7: case 8: case 9: case 10: case 11:
					minServeTime = 1;
					maxServeTime = 4;
					minSpawnTime = 2; 
					maxSpawnTime = 4;
					paydesks = 2;
					break;
				case 12: case 13: case 14: case 15:
					minServeTime = 1;
					maxServeTime = 4;
					minSpawnTime = 4;
					maxSpawnTime = 6;
					paydesks = 1;
					break;
				case 16: case 17: case 18:
					minServeTime = 2;
					maxServeTime = 5;
					minSpawnTime = 2;
					maxSpawnTime = 4;
					paydesks = 3;
					break;
				case 19: case 20: case 21: case 22:
					minServeTime = 3;
					maxServeTime = 6;
					minSpawnTime = 1;
					maxSpawnTime = 3;
					paydesks = 4;
					break;
				default: 
					textArea_3.setText("Magazinul nu este deschis decat intre orele 7 - 22\nVa rugam incercati simularea pentru alta ora");
				}
				
				threadnr = 2*paydesks;
								
				JTextArea ta;
				ta = textArea_3;
				consumers = new PayDesk[paydesks];
				producers = new Producer[paydesks];
				threads = new Thread[threadnr];
				for (int i = 0; i < paydesks; i++){
					producers[i] = new Producer(new ArrayBlockingQueue<Client>(100), runtime, minSpawnTime, maxSpawnTime);
					consumers[i] = new PayDesk(producers[i].getQueue(), runtime, paydeskID, minServeTime, maxServeTime, ta);
					paydeskID++;
					}
			
				
				for (int i = 0; i < paydesks; i++){
					threads[i] = new Thread(producers[i]);		
					threads[i].start();
				}
				int j = 0;
				for (int i = paydesks; i < threadnr; i++){
					threads[i] = new Thread(consumers[j]);		
					threads[i].start();
					j++;
				}
				
				return true;
			}

			@Override
			protected void done() {
			
				String txt3 = new String();
				
				for (int i = 0; i < threadnr; i++){
					try {
						threads[i].join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			
				for (int i = 0 ; i < paydesks; i++){
				
						txt3 = txt3 + consumers[i].howManyClients() + "\n" + consumers[i].meanServeTime() + "\n" + consumers[i].emptyTime() + "\n\n";
					
					  
				}	
				setTA3(txt3);
				super.done();
			}	
			
		};
		worker.execute();
		
	}
	
	public SwingWorker<Boolean, String> getWorker(){
		return worker;
	
	}
	
	public void setSimHour(int hour){ 
		this.hour = hour;
	}
	
	public void setRuntime(int runtime){
		this.runtime = runtime;
	}
	
	public void setMinServeTime(int time){
		this.minServeTime = time;
	}
	
	public void setMaxServeTime(int time){
		this.maxServeTime = time;
	}
	
	public void setMinSpawnTime(int time){
		this.minSpawnTime = time;
	}
	
	public void setMaxSpawnTime(int time){
		this.maxSpawnTime = time;
	}
	
	public void setIdPayDesk(int id){
		this.idPaydesk = id;
	}
	
	public void setNoPaysdesk(int no){
		this.nrPaydesk = no;
	}
	
	public static int getHour(){
		return Integer.parseInt(txtInsertTheHour.getText());
	}
	
	public static int getRuntime(){
		return Integer.parseInt(txtInsertTheAmount.getText());
	}
	
	public static synchronized void setTA3(String a) { 
		textArea_3.append(a);
	}
		
	public static JTextArea getTA3(){
		return textArea_3;
	}
	
	public JCheckBox getCheck(){
		return chckbxSimulateWithUser;
	}
	
	public JTextField getTxt(){
		return textField;
	}
	
	public JTextField getTxt1(){
		return textField_1;
	}
	
	public JTextField getTxt2(){
		return textField_2;
	}
	
	public JTextField getTxt3(){
		return textField_3;
	}
	
	public JTextField getTxt4(){
		return textField_4;
	}
}
