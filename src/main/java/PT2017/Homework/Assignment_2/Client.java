package PT2017.Homework.Assignment_2;
import java.util.Date;

public class Client {

	private int ID; 
	private long arrivalTime; 
	private long departureTime;
	
	public Client (int ID, Date arrivalTime){
		this.ID = ID;
		this.arrivalTime = new Date().getTime();
	}
	
	public int getID(){
		return this.ID;
	}
	
	public long getArrivalTime(){
		return this.arrivalTime;
	}
	
	public void setDepartureTime(long time){
		this.departureTime = time;
	}
	
	public long getDepartureTime(){
		return this.departureTime;
	}
	
	public String toString(){
		return new String(Integer.toString(ID));
	}
	
}
